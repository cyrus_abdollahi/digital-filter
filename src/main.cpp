/** 
 *  @file 
 *  @brief     Generates a sinusoidal test input in the body frame and
 *             exercises a second order digital filter. The filtered output
 *             is then converted to the local level NAV frame (with wander
 *             angle already factored in) and logged to an output file. This
 *             file can then be loaded in MATLAB and compared to the 
 *             simulink results.
 *  @copyright 
 *  Permission is hereby granted, free of charge, to any person 
 *  obtaining a copy of this software and associated documentation files (the 
 *  "Software"), to deal in the Software without restriction, including without 
 *  limitation the rights to use, copy, modify, merge, publish, distribute, 
 *  sublicense, and/or sell copies of the Software, and to permit persons to 
 *  whom the Software is furnished to do so, subject to the following 
 *  conditions:
 *  <br> <br>
 *  The above copyright notice and this permission notice shall be included in 
 *  all copies or substantial portions of the Software.
 *  <br> <br>
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 *  IN THE SOFTWARE. 
 *  <br> <br>
 *  2021 Cyrus Abdollahi 
 */


/*! \mainpage Welcome!
 *
 * This project is a simple demonstration of how to implement a 2nd-order 
 * lowpass and highpass filter. The intent is to demonstrate best coding 
 * practices as part of my code porfolio. All code adheres to the Google 
 * C++ style guidelines. Visual Studio Code was used to create this project, 
 * along with the Eigen and Boost template libraries. The code was documented 
 * using Doxygen was also created to document the code. Unit test code is 
 * provided in Matlab to compare and validate the C++ results.
 */ 

#define _USE_MATH_DEFINES
#include <cmath>
#include <fstream>
#include <iostream>

#include <Eigen/Dense>
#include <boost/circular_buffer.hpp>
#include <boost/math/constants/constants.hpp>
#include "filter.hpp"

// formatting specifiers for logging output data to text file.
const int    kHeaderWidth           = 18;
const int    kClockPrecision        = 3;
const int    kAccelPrecision        = 5;

// Conversion constants
const double kRps2Hz                = 2.0 * 
                                           boost::math::constants::pi<double>();

// Simulation rate and stop time values
const double kSamplePeriodSec   = 0.02;
const double kSampleRateHz      = 1.0 / kSamplePeriodSec;
const double kSampleRateRps     = kRps2Hz * kSampleRateHz;
const double kNyquistRateHz     = 0.5 * kSampleRateHz;
const double kNyquistRateRps    = 0.5 * kSampleRateRps;
const double kTerminalTimeSec   = 20.0;
const int    kStartTick         = 0;
const int    kStopTick          = kTerminalTimeSec * kSampleRateHz;
                             
// Test input u = sin( time * 2 * pi /kSinePeriod )
const double kSinePeriodSec     = 4.0;
const double kSineFreqHz        = 1.0 / kSinePeriodSec;
const double kSineFreqRps       = kRps2Hz * kSineFreqHz;

// Define default filter constants
const double kFilterCutoffHz    = 0.275;
const double kNormalizedCutoff  = kFilterCutoffHz / kNyquistRateHz;

// Body to nav unit test matrix
const Eigen::Matrix3d C_NB((Eigen::Matrix3d() <<
                                              0.866,  0.250, 0.433,
                                             -0.500,  0.433, 0.750,
                                              0.000, -0.866, 0.500).finished());

//|----------------------------------------------------------------------------
//| program main
//|----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    if (argc == 1 || argc > 2)
    {
        std::cerr << "User did not specify an input file. Please run --help for"
                  << " usage.\n";
        exit(EXIT_FAILURE);
    }
    else if (argc == 2 && strcmp(argv[1], "--help") == 0)
    {
        std::cout << "\nUsage: <filename>.txt to record outputs to in the " 
                  << "\\output directory. \n\n";
        return 0;
    }
    
    // open file to write the output data
    std::ofstream fOut(argv[1]);

    if (!fOut.is_open())
    {
        std::cerr << "Error in file " << __FILE__ << " on line " << __LINE__
                  << "\n" << "Cannot open file: " << argv[1] << "\n";
        exit(EXIT_FAILURE);
    }

    // Print column headers to the output file once.
    fOut << "Time (sec)"
         << std::setfill(' ') << std::setw(kHeaderWidth)
         << "Raw Accel (mps)"
         << std::setfill(' ') << std::setw(kHeaderWidth)
         << "North (mps2)"
         << std::setfill(' ') << std::setw(kHeaderWidth)
         << "East (msp2)"
         << std::setfill(' ') << std::setw(kHeaderWidth)
         << "Down (mps2)"
         << std::setfill(' ') << std::setw(kHeaderWidth)
         << "Body X (mps2)"
         << std::setfill(' ') << std::setw(kHeaderWidth)
         << "Body Y (msp2)"
         << std::setfill(' ') << std::setw(kHeaderWidth)
         << "Body Z (mps2)\n";

    // Instantiate aceleration vectors.
    Eigen::Vector3d filtered_accel_body_mps2 = Eigen::Vector3d::Zero();
    Eigen::Vector3d filtered_accel_nav_mps2  = Eigen::Vector3d::Zero();
    
    try 
    {
    digital_filter::SecondOrderIIR DigitalFilter = 
    digital_filter::SecondOrderIIR::make(
                          kNormalizedCutoff, 
                          kNyquistRateRps, 
                          digital_filter::SecondOrderIIR::FilterType::kLowPass);
    
    // Execute simulation loop.
    for (int timeTicks = kStartTick; timeTicks <= kStopTick; timeTicks++)
    {
        double system_time_sec       = timeTicks * kSamplePeriodSec;
        double test_input_accel_mps2 = sin(kSineFreqRps * system_time_sec);
        
        // Digitally filter the signal
        filtered_accel_body_mps2.x() = 
                                    DigitalFilter.Tick(test_input_accel_mps2);

        // Rotate accelerations into the NAV frame
        filtered_accel_nav_mps2 = C_NB.transpose() * filtered_accel_body_mps2;

        // Print output values always.
        fOut << std::fixed << std::left 
             << std::setprecision(kClockPrecision)
             << std::setw(kHeaderWidth) << system_time_sec
             << std::setprecision(kAccelPrecision)
             << std::setw(kHeaderWidth) << test_input_accel_mps2
             << std::setw(kHeaderWidth) << filtered_accel_nav_mps2.x()
             << std::setw(kHeaderWidth) << filtered_accel_nav_mps2.y()
             << std::setw(kHeaderWidth) << filtered_accel_nav_mps2.z()
             << std::setw(kHeaderWidth) << filtered_accel_body_mps2.x()
             << std::setw(kHeaderWidth) << filtered_accel_body_mps2.y()
             << std::setw(kHeaderWidth) << filtered_accel_body_mps2.z() << "\n";
    }
    }
    catch (std::range_error &e)
    {
        std::cerr << e.what() << std::endl;
    }

    // Perform cleanup, close all files.
    fOut.close();

    return EXIT_SUCCESS;
}