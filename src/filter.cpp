/** 
 *  @file 
 *  @brief Implements a simple second order digital filter in the z-domain.
 *  @copyright 
 *  Permission is hereby granted, free of charge, to any person 
 *  obtaining a copy of this software and associated documentation files (the 
 *  "Software"), to deal in the Software without restriction, including without 
 *  limitation the rights to use, copy, modify, merge, publish, distribute, 
 *  sublicense, and/or sell copies of the Software, and to permit persons to 
 *  whom the Software is furnished to do so, subject to the following 
 *  conditions:
 *  <br> <br>
 *  The above copyright notice and this permission notice shall be included in 
 *  all copies or substantial portions of the Software.
 *  <br> <br>
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 *  IN THE SOFTWARE. 
 *  <br> <br>
 *  2021 Cyrus Abdollahi 
 */
#define _USE_MATH_DEFINES
#include <cfloat>
#include <cmath>
#include <iostream>
#include <utility>

#include "filter.hpp"

namespace digital_filter 
{
    constexpr double SecondOrderIIR::kMinNormalizedCutoffFrequency;
    constexpr double SecondOrderIIR::kMaxNormalizedCutoffFrequency;

    SecondOrderIIR SecondOrderIIR::make(
                                   const double     normalized_cutoff_frequency,
                                   const double     nyquist_rate_rps,
                                   const FilterType filter_type)
{
    digital_filter::SecondOrderIIR Filter; 
    Filter.set_filter_type(filter_type);
    Filter.set_nyquist_rate_rps(nyquist_rate_rps);
    Filter.set_normalized_cutoff_frequency(normalized_cutoff_frequency); 

    return Filter;
}

SecondOrderIIR::Coefficients SecondOrderIIR::get_coefficients(void) 
{
    return coefficients_;
}

double SecondOrderIIR::get_cutoff_frequency_rps(void) 
{
    return cutoff_frequency_rps_;
}

double SecondOrderIIR::get_normalized_cutoff_frequency(void) 
{
    return normalized_cutoff_frequency_;
}

double SecondOrderIIR::get_nyquist_rate_rps(void) 
{
    return nyquist_rate_rps_;
}

double SecondOrderIIR::get_sample_rate_rps(void) 
{
    return sample_rate_rps_;
}

void SecondOrderIIR::set_nyquist_rate_rps(const double nyquist_rate_rps)
{
    if ( nyquist_rate_rps <= DBL_EPSILON  )
    {
        throw std::range_error("Nyquist must be greater than zero.\n");
    }
    else
    {
        nyquist_rate_rps_ = nyquist_rate_rps;
        ComputeFilterCoefficients();
    }
}

void SecondOrderIIR::set_normalized_cutoff_frequency(
                                       const double normalized_cutoff_frequency)
{
    if (normalized_cutoff_frequency <= kMinNormalizedCutoffFrequency || 
        normalized_cutoff_frequency >= kMaxNormalizedCutoffFrequency)
    {
        throw std::range_error("Cutoff frequency violates Nyquist criteria.\n");
    }
    else
    {
        normalized_cutoff_frequency_ = normalized_cutoff_frequency;
        ComputeFilterCoefficients();
    }
}

void SecondOrderIIR::set_filter_type(const FilterType filter_type)
{
    if (filter_type == FilterType::kLowPass || 
        filter_type == FilterType::kHighPass)
    {
        filter_type_ = filter_type;
        ComputeFilterCoefficients();
    }
    else
    {
        throw std::range_error("Invalid filter enumeration type.\n");
    }
}
 
void SecondOrderIIR::Reset(void)
{
    input_history_.assign(kOrder, 0.0);
    output_history_.assign(kOrder, 0.0);
}

double SecondOrderIIR::Tick(const double raw_input) 
{
    double filter_output = coefficients_.b0 * raw_input               +
                           coefficients_.b1 * input_history_.front()  +
                           coefficients_.b2 * input_history_ .back()  -
                           coefficients_.a1 * output_history_.front() -
                           coefficients_.a2 * output_history_.back();
    
    // update buffers 
    input_history_.push_front(raw_input);
    output_history_.push_front(filter_output);

    return filter_output;
} 

void SecondOrderIIR::ComputeFilterCoefficients(void)
{
    sample_rate_rps_      = 2.0 * nyquist_rate_rps_;
    cutoff_frequency_rps_ = normalized_cutoff_frequency_ * nyquist_rate_rps_;

    // Compute commonly used variables in coefficient generation
    double cutoff_frequency_rps_squared = cutoff_frequency_rps_ *
                                          cutoff_frequency_rps_;
    
    // Apply bilinear transform to cutoff frequency
    double k                            = cutoff_frequency_rps_ / 
                                          tan(M_PI * cutoff_frequency_rps_ / 
                                              sample_rate_rps_);
    
    double k_squared                    = k * k;
    
    // Division by zero is protected during setting of these parameters.
    double denominator                  = cutoff_frequency_rps_squared + 
                                          k_squared + sqrt(2.0) * k *
                                          cutoff_frequency_rps_;

    // Demominator coefficients are common to both lowpass and highpass filters
    // so compute them only once.
    coefficients_.a1 = (2.0 * cutoff_frequency_rps_squared - 2.0 * k_squared) 
                        / denominator;
    coefficients_.a2 = (cutoff_frequency_rps_squared + k_squared - 
                        sqrt(2.0) * k *cutoff_frequency_rps_) / denominator; 

    // Compute coefficients based on filter type:
    switch (filter_type_)
    {
        case FilterType::kLowPass:
            coefficients_.b0 = cutoff_frequency_rps_squared / denominator;
            coefficients_.b1 = 2.0 * coefficients_.b0;
            coefficients_.b2 = coefficients_.b0;                     
            break;

        case FilterType::kHighPass:
            coefficients_.b0 = k_squared / denominator;
            coefficients_.b1 = -2.0 * coefficients_.b0;
            coefficients_.b2 = coefficients_.b0;                     
            break;
    }
}
} // namespace digital_filter