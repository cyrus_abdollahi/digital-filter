/** 
 *  @file 
 *  @brief Implements a simple second order digital filter in the z-domain.
 *  @copyright 
 *  Permission is hereby granted, free of charge, to any person 
 *  obtaining a copy of this software and associated documentation files (the 
 *  "Software"), to deal in the Software without restriction, including without 
 *  limitation the rights to use, copy, modify, merge, publish, distribute, 
 *  sublicense, and/or sell copies of the Software, and to permit persons to 
 *  whom the Software is furnished to do so, subject to the following 
 *  conditions:
 *  <br> <br>
 *  The above copyright notice and this permission notice shall be included in 
 *  all copies or substantial portions of the Software.
 *  <br> <br>
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 *  IN THE SOFTWARE. 
 *  <br> <br>
 *  2021 Cyrus Abdollahi 
 */
#ifndef FILTER_HPP
#define FILTER_HPP

#include <cfloat>

#include <boost/circular_buffer.hpp>

namespace digital_filter 
{

class SecondOrderIIR final {
 public:

  enum class FilterType {kLowPass, kHighPass};

  struct Coefficients {
  Coefficients(void) : a1(0.0), a2(0.0), b0(0.0), b1(0.0), b2(0.0) {}
  Coefficients(double a1, double a2, double b0, double b1, double b2) : 
               a1(a1), a2(a2), b0(b0), b1(b1), b2(b2) {}
    double a1{0.0};
    double a2{0.0};
    double b0{0.0};
    double b1{0.0};
    double b2{0.0};
  };

  /** 
   * @brief  Factory function to safely make an object and test input validity.
   * @param normalized_cutoff_frequency (unitless)
   * @param nyquist_rate_rps (rad/s)
   * @param filter_type (enum)
   * @return SecondOrderIIR object
  */
  static SecondOrderIIR make(const double     normalized_cutoff_frequency,
                             const double     nyquist_rate_rps,
                             const FilterType filter_type);
  
  /** 
   * @brief  Get the filter coefficients.
   * @return Coefficients structure
  */
  Coefficients get_coefficients(void);
  
  /** 
   * @brief  Get the filter cutoff frequency.
   * @return Cutoff Freq. rad/s
  */
  double get_cutoff_frequency_rps(void);

  /** 
   * @brief  Get the filter normalized cutoff frequency [0,fsampling/2]
   * @return Normalized Freq. (unitless)
  */
  double get_normalized_cutoff_frequency(void);

  /** 
   * @brief  Get the filter Nyquist rate (sampling rate / 2)
   * @return Nyquist rate rad/s
  */
  double get_nyquist_rate_rps(void);

  /** 
   * @brief  Get the filter sampling rate.
   * @return Sampling rate rad/s
  */
  double get_sample_rate_rps(void);

  /**
   * @brief  Setter function for the filter Nyquest rate (half sampling rate).
   *         Will throw an exception if the sampling rate is less than zero. 
   *         Calling the set funciton will automatically update the filter 
   *         coefficients.
   * @param  Nyquist rate rad/s
   */
  void set_nyquist_rate_rps(const double nyquist_rate_rps_);

  /**
   * @brief  Setter function for the normalized cutoff frequency (fc/fnyquist).
   *         Cutoff frequency cannot be greater than half the nyquist rate or 
   *         filter aliasing will occur. This is prevented by throwing an 
   *         exception to the calling function.
   * @param  cutoff (unitless ratio)
   */
  void set_normalized_cutoff_frequency(
                                      const double normalized_cutoff_frequency);

  /**
   * @brief  Setter function for the filter type. This will thorw an exception
   *         if the user tries to set a filter type not part of the enum list.
   * @param  filter type enumerator
   */
  void set_filter_type(const FilterType filter_type);

  /**
   * @brief  Resets the filter buffer.
   */
  void Reset(void);

  /** 
   * @brief  Implements a "tick" in time of digital filter in the Z-domain.
   * @note  
     \verbatim 
      Y(Z)              b0 + b1 Z^-1 + b2 * Z^-2  
      ----  = G(Z) =  ----------------------------
      U(Z)               1 + a1 Z^-1 + a2 * Z^-2 
     \endverbatim
   * @param  raw_input agnostic to units! 
   * @return filtered value
  */
  double Tick(const double raw_input);

 private:

  // Filter order sets the buffer size  
  static constexpr size_t kOrder{2};        
  
  // Initializer constants for safe construction                  
  static constexpr double kMinNormalizedCutoffFrequency{DBL_EPSILON};
  static constexpr double kMaxNormalizedCutoffFrequency{1.0};
  static constexpr double kInitialNyquestRateRps{1.0};
  static constexpr double kInitialNormalizedCutoffFrequency{0.1}; 
 
  double nyquist_rate_rps_{kInitialNyquestRateRps};
  double sample_rate_rps_{2.0 * nyquist_rate_rps_}; 
  double normalized_cutoff_frequency_{kInitialNormalizedCutoffFrequency};
  double cutoff_frequency_rps_{normalized_cutoff_frequency_ * 
                               nyquist_rate_rps_};  

  Coefficients coefficients_;
  FilterType filter_type_{FilterType::kLowPass};
  boost::circular_buffer<double> input_history_{kOrder, 0.0};
  boost::circular_buffer<double> output_history_{kOrder, 0.0};
  
  /** 
   * @brief Computes the filter coefficients based on member variable frequency 
   * and sampling rate.
  */
  void ComputeFilterCoefficients(void);
};
} // namespace digital_filter
#endif // FILTER_HPP