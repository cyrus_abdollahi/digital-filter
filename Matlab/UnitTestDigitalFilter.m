%--------------------------------------------------------------------------
% File Name   : UnitTestDigitalFilter.m
%
% Description : Performs unit test verification of C++ implementation of a 
%               second order digital filter against MATLAB/Simulink.
%
% Version     : 1.0
%
% Copyright 2021 Cyrus Abdollahi 
%
% Revision History :
% Date       JIRA    Author         Summary of Change
% ---------  ------  -------------  -------------------------
% 06/04/18   N/A     C. Abdollahi   Original Development
% 07/29/21           C. Abdollahi   Revised input file parsing
%--------------------------------------------------------------------------
clear all; close all; clc;

%% Define enum like constants
kRoll = 1; kPitch = 2; kYaw  = 3;
kX    = 1; kY     = 2; kZ    = 3;

%% Load C++ data from output file

% Cpp file name
cpp_file_name = '../output/filtered_acceleration.txt';

% Load C++ output file into the workspace
raw_cpp_data = importdata(cpp_file_name);

% Place data into struct
cpp_data.time        =  raw_cpp_data.data(:,1);
cpp_data.raw_accel   =  raw_cpp_data.data(:,2);
cpp_data.accel_nav   = [raw_cpp_data.data(:,3), ...
                        raw_cpp_data.data(:,4), ...
                        raw_cpp_data.data(:,5)];
cpp_data.accel_body  = [raw_cpp_data.data(:,6), ...
                        raw_cpp_data.data(:,7), ...
                        raw_cpp_data.data(:,8)];
                    
%% Generate test input
rps_2_Hz             = 2.0 * pi;
Hz_2_rps             = 1.0 / rps_2_Hz;
sampleRate_Hz        = 50;
NyquistRate_Hz       = sampleRate_Hz / 2;
testPeriod_sec       = 4.0;
testFreq_Hz          = 1.0 / testPeriod_sec;

% Generate 2nd order lowpass filter coefficients 
filter_order         = 2;
frequency_cutoff_Hz  = 1.1 * testFreq_Hz;
[num_TF,den_TF]      = butter(filter_order, frequency_cutoff_Hz / NyquistRate_Hz);
sys                  = tf(num_TF,den_TF, 1.0/sampleRate_Hz);
 
% Generate Bode plot response
options = bodeoptions;
options.FreqUnits = 'Hz';
bode(sys, options); grid on;

% Verify stability
zplane(num_TF,den_TF); grid on;

fprintf('Test Freq is (%2.3f) Hz, cutoff Freq is (%2.3f) Hz\n', testFreq_Hz, Hz_2_rps * bandwidth(sys));

% Body to navigation DCM.
DCM_Body_Nav = [0.866 0.25 0.433; -0.5 0.433 0.75; 0 -0.866 0.5];


%% Run the simulink unit test
sim('ZFilter');

%% Plot formatting
cpp_data.linewidth            = 1.0;
cpp_data.raw_color            = 'r';
cpp_data.filtered_color       = 'k';
cpp_data.unfiltered_linestyle = '--';
matlab_data.filtered_color    = 'm';
labels                        = {'Roll Axis m/s^2', 'Pitch Axis m/s^2', 'Yaw Axis m/s^2'};

%% Plot the body frame accelerations

iFig = 2; h = figure(iFig); set(h,'WindowStyle','docked','numbertitle','off','name','Body Frame');
for (iAxis = 1:3)
    ax{iAxis} = subplot(3,1,iAxis); hold on;
    plot(cpp_data.time, cpp_data.accel_body(:,iAxis), cpp_data.filtered_color);
    plot(filtered_accel_body.time, filtered_accel_body.signals.values(:,iAxis), matlab_data.filtered_color);
    set(ax{iAxis},'XMinorTick','on','YMinorTick','on')
    ylabel(labels{iAxis}); 
    grid on;
    ylim([-1 1]);  
    
    switch iAxis
        case kRoll
            title('Body Acceleration')
            plot(cpp_data.time, cpp_data.raw_accel, cpp_data.raw_color, 'linestyle', cpp_data.unfiltered_linestyle); 
            legend('Raw Accel','Filtered Accel');
        case kYaw
            xlabel('time sec');
    end
end

linkaxes([ax{kRoll}, ax{kPitch}, ax{kYaw}],'x');


%% Plot the nav frame accelerations

labels = {'X m/s^2', 'Y m/s^2', 'Z m/s^2'};

iFig = iFig + 1; h = figure(iFig); set(h,'WindowStyle','docked','numbertitle','off','name','Nav Frame');
for (iAxis = 1:3)
    ax{iAxis} = subplot(3,1,iAxis); hold on;
    plot(cpp_data.time, cpp_data.accel_nav(:,iAxis), cpp_data.filtered_color);
    plot(filtered_accel_nav.time, filtered_accel_nav.signals.values(:,iAxis), cpp_data.filtered_color, 'linestyle', '--');
    set(ax{iAxis},'XMinorTick','on','YMinorTick','on')
    ylabel(labels{iAxis}); 
    grid on;
    ylim([-1 1]);  
    
    switch iAxis
        case kX
            title('NAV Acceleration')
        case kZ
            xlabel('time sec');
    end
end

linkaxes([ax{kRoll}, ax{kPitch}, ax{kYaw}],'x');

%% Plot the Error deltas between the two models:

labels = {'Err X m/s^2', 'Err Y m/s^2', 'Err Z m/s^2'};

iFig = iFig + 1; h = figure(iFig); set(h,'WindowStyle','docked','numbertitle','off','name','Nav Errors');
for (iAxis = 1:3)
    ax{iAxis} = subplot(3,1,iAxis); hold on;
    error = filtered_accel_nav.signals.values(:,iAxis) - cpp_data.accel_nav(:,iAxis);
    plot(cpp_data.time, error, cpp_data.filtered_color);
    set(ax{iAxis},'XMinorTick','on','YMinorTick','on')
    ylabel(labels{iAxis}); 
    grid on;
    ylim([-1 1]);  
    
    switch iAxis
        case kX
            title('Filter Unit Test Error Residuals')
        case kZ
            xlabel('time sec');
    end
end

linkaxes([ax{kRoll}, ax{kPitch}, ax{kYaw}],'x');

