var indexSectionsWithContent =
{
  0: "_abcdfgkmrstw",
  1: "cs",
  2: "d",
  3: "fm",
  4: "cgmrst",
  5: "abk",
  6: "f",
  7: "k",
  8: "_",
  9: "w"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

