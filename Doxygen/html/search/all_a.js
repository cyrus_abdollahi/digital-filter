var searchData=
[
  ['secondorderiir_40',['SecondOrderIIR',['../classdigital__filter_1_1_second_order_i_i_r.html',1,'digital_filter']]],
  ['set_5ffilter_5ftype_41',['set_filter_type',['../classdigital__filter_1_1_second_order_i_i_r.html#aec9449b60f9c6121e2b9f46b81345bbd',1,'digital_filter::SecondOrderIIR']]],
  ['set_5fnormalized_5fcutoff_5ffrequency_42',['set_normalized_cutoff_frequency',['../classdigital__filter_1_1_second_order_i_i_r.html#a2bdaaac5070491429e4c363660632b6e',1,'digital_filter::SecondOrderIIR']]],
  ['set_5fnyquist_5frate_5frps_43',['set_nyquist_rate_rps',['../classdigital__filter_1_1_second_order_i_i_r.html#a9ea54fe7eef3a16b16646acc92fafe5d',1,'digital_filter::SecondOrderIIR']]]
];
