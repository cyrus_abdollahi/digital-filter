var searchData=
[
  ['get_5fcoefficients_54',['get_coefficients',['../classdigital__filter_1_1_second_order_i_i_r.html#a3a6fcc3a96bf6984c313c6f78079d972',1,'digital_filter::SecondOrderIIR']]],
  ['get_5fcutoff_5ffrequency_5frps_55',['get_cutoff_frequency_rps',['../classdigital__filter_1_1_second_order_i_i_r.html#a3f9f2216b4b0c57b57680dc473a8e973',1,'digital_filter::SecondOrderIIR']]],
  ['get_5fnormalized_5fcutoff_5ffrequency_56',['get_normalized_cutoff_frequency',['../classdigital__filter_1_1_second_order_i_i_r.html#a671b298c401d53cdf3179ccb2fd6b260',1,'digital_filter::SecondOrderIIR']]],
  ['get_5fnyquist_5frate_5frps_57',['get_nyquist_rate_rps',['../classdigital__filter_1_1_second_order_i_i_r.html#a4896ee7e3bbcd43269369d4f81c2aad4',1,'digital_filter::SecondOrderIIR']]],
  ['get_5fsample_5frate_5frps_58',['get_sample_rate_rps',['../classdigital__filter_1_1_second_order_i_i_r.html#a9eb20694882f1f275540a08513a919fa',1,'digital_filter::SecondOrderIIR']]]
];
