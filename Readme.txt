-------------------------------------------------------------------------------
                               Run instructions
-------------------------------------------------------------------------------
[1] To run this demo, you will need to download and install the Eigen math 
library package here:  https://eigen.tuxfamily.org/index.php?title=Main_Page

[2] Next, update the tasks.json file to point to the location you downloaded 
Eigen, see below:

    "tasks": [
        {
            "label": "build",
            "command": "g++",
            "args": [
                "-g",
                "main.cpp",
                "filter.cpp",
                "-I",
                "</YOUR PATH TO EIGEN ROOT HERE>",
                "-std=c++11",
                "-o",
                "DigitalFilter"
            ],

[3] Generate a \bin and directory at the root level 

[4] Compile and run the executable DigitalFilter. It will generate an output
file called filtered_acceleration.txt that can be read in and processed in
MATLAB or Python.
